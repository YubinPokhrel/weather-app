import React, { Component } from 'react';
import axios from 'axios';
import Clear from '../Image/clear.png'
import Rain from '../Image/rain.png'
import clouds from '../Image/clouds.png'
import humid from '../Image/humidity.png'
import wind from '../Image/wind.png'
import latitude from '../Image/latitude.png'
import longitude from '../Image/longitude.png'
import temp_min from '../Image/temp_min.png'
import temp_max from '../Image/temp_max.png'
import sunrise from '../Image/sunrise.png'
import sunset from '../Image/sunset.png'

import Style from './Style.css'

class Weather extends Component {
   
    state={
        weatherData:[],
       date:""
        
    }

    componentDidMount(){
        this.getDate(); 
    }

    getDate =() => {
        var date = new Date().toDateString();
        this.setState({
            date:date
        });
        
    }


        
    getDataFromAPI =(city, country) => {
        // let self = this;
       let url = 'https://api.openweathermap.org/data/2.5/weather?q='+city+','+country+'&appid=8d2de98e089f1c28e1a22fc19a24ef04'
        axios.get(url)
        .then ((response) => {
            this.setState({
                weatherData:response.data
                
            })
           
            const {country, sunrise, sunset} = response.data.sys;
            const {temp_min, temp_max, pressure, humidity, temp} = response.data.main;
            const {lon, lat} = response.data.coord;
            const {speed}= response.data.wind;
            const {main}= response.data.weather[0];
            const {name}= response.data;
        
            this.setState({
                weatherData:{
                    city_name:name,
                    country_name:country,
                    temperature_minimum:(temp_min -273.15).toFixed(2),
                    temperature_maximum:(temp_max -273.15).toFixed(2),
                    sunrise_hours: (new Date(sunrise * 1000)).getHours(),
                    sunrise_minute:(new Date(sunrise * 1000)).getMinutes(),
                    sunset_hours: (new Date(sunset * 1000)).getHours(),
                    sunset_minute:(new Date(sunrise * 1000)).getMinutes(),
                    humidity:humidity,
                    wind_speed: speed,
                    situation: main,
                    longitude: (lon).toFixed(2),
                    latitude:(lat).toFixed(2),
                    temperature: (temp-273.15).toFixed(),   
                }
            })
        })
        .catch((error) => {
            console.log(error)
        })
    
    }

    changeWeatherImage() {
        let condition = this.state.weatherData.situation;
        
        if(condition === "Clouds")
        {
           return <img src={clouds}/>
        } 
        else if (condition === "Rain")
        {
            return <img src={Rain}/>
        } 
        else if(condition === "Clear")
         {
           return <img src={Clear}/>
        } else {
            return null
        }

    }
    handleChange(e){
        let{name, value} = e.target;
        let{weatherData} = this.state;
        weatherData[name] = value;
        this.setState({
            weatherData
            
        })
    
    }
    render() {
      
        const {date} = this.state;
        return (
        
            <div className="container" style={{width:1200, display:"flex"}}>
            <div style={{marginTop:200, color: "Yellow", fontWeight:700 }}>
                    <div className="mb-4" id="country">
                        <label className="label label-primary">Country Name: &nbsp;</label>
                        <input type="text" 
                        name="country" 
                        className="form-control-sm"
                        placeholder="Country Name" 
                        value={this.state.weatherData.country}
                         onChange={(e) => this.handleChange(e)}/>
                    </div>

                    <div className="mb-4" id= "city">
                        <label  className="label label-primary">City: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	&nbsp;</label>
                        <input type="text" 
                        name ="city"
                        className="form-control-sm"  
                        placeholder="City Name"
                        value={this.state.weatherData.name}
                         onChange={(e) => this.handleChange(e)}
                        />
                    </div>
                    <div>
                    <button type="button" className="btn btn-success" onClick={() =>this.getDataFromAPI(this.state.weatherData.city, this.state.weatherData.country)}>Get Weather</button>
                    </div>
                    <p style={{color:"yellow", fontSize:20, marginTop:20}}>{date}</p>
                    <div style={{ display:"flex", alignItems:"center"}}><img src={clouds} width="120"/>
                       
                    <p style={{float:"right",fontSize:60, color: "yellow", fontWeight:700}}>{this.state.weatherData.temperature}°C</p>
                    </div>
                    <div>
                    <p style={{fontSize:30, fontWeight:700}}>{this.state.weatherData.situation}</p>
                    </div>
                    <div>
                    <p style={{fontSize:20, fontWeight:500}}>{this.state.weatherData.city_name}, {this.state.weatherData.country_name}</p>
                    </div>
                    </div>
                 {/* --------------------cards----------------- */}
                 <div style={{marginTop:50, marginLeft:50}}>
                 <h1 style={{fontSize:50, fontWeight:1000, color:"Yellow"}}>Weather App</h1>
                 <div style={{width:750,marginTop:10 }}>
<div className="row w-200" >
        <div className="col-md-3" >
            <div className="card border-info mx-sm-1 p-3" >                <div><img src ={humid} height="40" width="40"/></div>
                <div className="text-info text-center mt-3"><h5 style={{color:"#FFF"}}>Humidity</h5></div>
                <div className="text-info text-center mt-2"><h3 style={{color:"#EBF2FE"}}>{this.state.weatherData.humidity}%</h3></div>
            </div>
        </div>
        <div className="col-md-3">
            <div className="card border-info mx-sm-1 p-3 " >
            <div><img src ={wind} height="40" width="80"/></div>
                <div className="text-success text-center mt-3" ><h5 style={{color:'#FFF'}}>Wind</h5></div>
                <div className="text-success text-center mt-2"><h3 style={{color:"#EBF2FE"}}>{this.state.weatherData.wind_speed}mph</h3></div>
            </div>
        </div>
        <div className="col-md-3">
            <div className="card border-info mx-sm-1 p-3 " >
            <div><img src ={longitude} height="40" width="40"/></div>
                <div className="text-success text-center mt-3" ><h5 style={{color:'#FFF'}}>Longitude</h5></div>
                <div className="text-success text-center mt-2"><h3 style={{color:"#EBF2FE"}}>{this.state.weatherData.longitude}°E</h3></div>
            </div>
        </div>
        <div className="col-md-3">
            <div className="card border-info mx-sm-1 p-3 " >
            <div><img src ={latitude} height="40" width="40"/></div>
                <div className="text-success text-center mt-3" ><h5 style={{color:'#FFF'}}>Latitude</h5></div>
                <div className="text-success text-center mt-2"><h3 style={{color:"#EBF2FE"}}>{this.state.weatherData.latitude}°N</h3></div>
            </div>
        </div>
     </div>
</div>
<div style={{width:750 ,marginTop:10}}>
<div className="row w-200" >
<div className="col-md-3">
            <div className="card border-info mx-sm-1 p-3" >
            <div><img src ={temp_min} height="40" width="40"/></div>
                <div className="text-success text-center mt-3" ><h5 style={{color:'#FFF'}}>Minimum Temperature</h5></div>
                <div className="text-success text-center mt-2"><h3 style={{color:"#EBF2FE"}}>{this.state.weatherData.temperature_minimum}°C</h3></div>
            </div>
        </div>
        <div className="col-md-3">
            <div className="card border-info mx-sm-1 p-3" >
            <div><img src ={temp_max} height="40" width="40"/></div>
                <div className="text-success text-center mt-3" ><h5 style={{color:'#FFF'}}>Maximum Temperature</h5></div>
                <div className="text-success text-center mt-2"><h3 style={{color:"#EBF2FE"}}>{this.state.weatherData.temperature_maximum}°C</h3></div>
            </div>
        </div>
        <div className="col-md-3">
            <div className="card border-info mx-sm-1 p-3" >
            <div><img src ={sunrise} height="40" width="40"/></div>
                <div className="text-success text-center mt-3" ><h5 style={{color:'#FFF'}}>Sunrise</h5></div>
                <div className="text-success text-center mt-2"><h3 style={{color:"#EBF2FE"}}>{this.state.weatherData.sunrise_hours}:{this.state.weatherData.sunrise_minute} A.M.</h3></div>
            </div>
        </div>
        <div className="col-md-3">
            <div className="card border-info mx-sm-1 p-3" >
            <div><img src ={sunset} height="40" width="40"/></div>
                <div className="text-success text-center mt-3" ><h5 style={{color:'#FFF'}}>Sunset</h5></div>
                <div className="text-success text-center mt-2"><h3 style={{color:"#EBF2FE"}}>{this.state.weatherData.sunset_hours}:{this.state.weatherData.sunset_minute} P.M.</h3></div>
            </div>
        </div>
     </div>
</div>
</div>
            </div>
              

                
        );
    }
}

export default Weather;